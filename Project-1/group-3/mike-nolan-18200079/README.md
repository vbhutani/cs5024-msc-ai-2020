# CS5024 - Theory & Practice of Advanced AI Ecosystems

## Project 1

**Student Name:** Michael Nolan

**Student ID:** 18200079

**Student Email:** 18200079@studentmail.ul.ie

---

Breakdown of files:

| Filename               | Description                             |
|------------------------|-----------------------------------------|
| `MN-AI.process`        | Your First AI - from module material    |
| `MNSubAI.process`      | The Sub AI - from module material       |
| `MNTopLevelAI.process` | The Top Level AI - from module material |
